package com.semperti.hipotecario.poc.fuse.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

import com.semperti.hipotecario.poc.fuse.model.PersonaIntegrador;
import com.semperti.hipotecario.poc.fuse.model.PersonasIntegrador;
import com.semperti.hipotecario.poc.fuse.model.TelefonoIntegrador;
import com.semperti.hipotecario.poc.fuse.model.TelefonosIntegrador;

import java.util.Iterator;

public class ServiceAggregator implements AggregationStrategy {
	private static final Logger logger = LoggerFactory.getLogger(ServiceAggregator.class);

	public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
		if (oldExchange == null)
			return newExchange;
		if (newExchange == null)
			return oldExchange;

		if (newExchange.getIn().getHeader("SERVICE_NAME", String.class).equals("COBIS"))
			return performAggregate(newExchange, oldExchange);

		return performAggregate(oldExchange, newExchange);
	}

	/** Cobis gana a Bup **/
	protected Exchange performAggregate(Exchange cobisExchange, Exchange bupExchange) {
		String operationName = cobisExchange.getIn().getHeader("operationName", String.class);
		Exchange returnExchange = cobisExchange;
		switch(operationName) {
			case "obtenerPersonas":
				aggregateObtenerPersonas(cobisExchange, bupExchange);
				break;

			case "obtenerPersona":
				fillPersona(cobisExchange.getIn().getBody(PersonaIntegrador.class), bupExchange.getIn().getBody(PersonaIntegrador.class));
				break;
			case "obtenerTelefonos":
				aggregateObtenerTelefonos(cobisExchange, bupExchange);
				break;

			case "obtenerTelefonosDePersona":
				aggregateObtenerTelefonos(cobisExchange, bupExchange);
				break;

			case "obtenerTelefono":
				fillTelefono(cobisExchange.getIn().getBody(TelefonoIntegrador.class), bupExchange.getIn().getBody(TelefonoIntegrador.class));
				break;

			case "obtenerPrestamoPatrimonial":
			case "obtenerPrestamosPatrimoniales":
			case "obtenerPrestamosPatrimonialesDePersona":
			case "obtenerTarjetaCreditoPatrimonial":
			case "obtenerTarjetasCreditoPatrimoniales":
			case "obtenerTarjetasCreditoPatrimonialesDePersona":
				break;

			case "obtenerDomicilio":
			case "obtenerDomicilios":
			case "obtenerDomiciliosDePersona":
				returnExchange = bupExchange;
				break;

			default:
				logger.error("Operation: '{}' desconocida", operationName);
		}

		return returnExchange;
	}

	protected void aggregateObtenerPersonas(Exchange cobisExchange, Exchange bupExchange) {
		PersonasIntegrador cobisPersonas = cobisExchange.getIn().getBody(PersonasIntegrador.class);
		PersonasIntegrador bupPersonas = bupExchange.getIn().getBody(PersonasIntegrador.class);

		for (PersonaIntegrador cobisPersona : cobisPersonas.getPersonas()) {
			for (Iterator<PersonaIntegrador> it = bupPersonas.getPersonas().iterator(); it.hasNext(); ) {
				PersonaIntegrador bupPersona = it.next();
				if (!cobisPersona.getId().equals(bupPersona.getId()))
					continue;

				fillPersona(cobisPersona, bupPersona);
				it.remove();
				break;
			}
		}

		// XXX: Se podrian agregar las peronas que sobraron
	}

	protected void fillPersona(PersonaIntegrador cobisPersona, PersonaIntegrador bupPersona) {
		cobisPersona.setEsPersonaFisica(bupPersona.getEsPersonaFisica());
		cobisPersona.setEsPersonaJuridica(bupPersona.getEsPersonaJuridica());
		cobisPersona.setValorLealtadCliente(bupPersona.getValorLealtadCliente());
        // cobisPersona.setNombres("Mi nombre");
	}


	protected void aggregateObtenerTelefonos(Exchange cobisExchange, Exchange bupExchange) {
		TelefonosIntegrador cobisTelefonos = cobisExchange.getIn().getBody(TelefonosIntegrador.class);
		TelefonosIntegrador bupTelefonos = bupExchange.getIn().getBody(TelefonosIntegrador.class);

		for (TelefonoIntegrador cobisTelefono : cobisTelefonos.getTelefonos()) {
			for (Iterator<TelefonoIntegrador> it = bupTelefonos.getTelefonos().iterator(); it.hasNext(); ) {
				TelefonoIntegrador bupTelefono = it.next();
				if (!cobisTelefono.getId().equals(bupTelefono.getId()))
					continue;

				fillTelefono(cobisTelefono, bupTelefono);

				it.remove();
				break;
			}
		}

		// XXX: Se podrian agregar las telefonos que sobraron
	}

	protected void fillTelefono(TelefonoIntegrador cobisTelefono, TelefonoIntegrador bupTelefono) {
		cobisTelefono.setPrioridad(bupTelefono.getPrioridad());
		cobisTelefono.setEsListaNegra(bupTelefono.getEsListaNegra());
        //bup le gana a cobis en el telefono
        cobisTelefono.setNumero(bupTelefono.getNumero());
	}
}
